﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class CaptureCamera : MonoBehaviour {

	WebCamTexture webCam = null;
	public Image immagine;
	public RenderTexture overviewTexture;
	public Texture2D tempImage;
	public Texture2D cornice;

	public Color32[] cols;
	public Color32[] colsAdd;

	void Start () 
	{
		if(webCam == null)
		{
			WebCamDevice[] devices = WebCamTexture.devices;

			if (devices.Length > 0)
			{
				webCam = new WebCamTexture (devices [0].name);

			}
				
		}

		immagine.material.mainTexture = webCam;

		webCam.Play();



		//byte[] bytesCornice = cornice.EncodeToPNG();
		//cornice.LoadImage(bytesCornice);
	


	}
	
	IEnumerator UploadPNG () 
	{
		tempImage = new Texture2D(webCam.width, webCam.height, TextureFormat.RGB24, false);
		cornice = new Texture2D(webCam.width, webCam.height, TextureFormat.RGB24, false);

		cornice = Resources.Load("camera") as Texture2D;
			
		cols = new Color32[webCam.width*webCam.height];
		colsAdd = new Color32[webCam.width*webCam.height];

		cols = webCam.GetPixels32();
//		colsAdd = cornice.GetPixels32();

		// additive 
/*		for(int i = 0; i < cols.Length; i++)
		{
			cols[i].r += colsAdd[i].r;
			cols[i].g += colsAdd[i].g;
			cols[i].b += colsAdd[i].b;
		}
*/
		
		tempImage.SetPixels32(webCam.GetPixels32());
		tempImage.Apply();

		byte[] bytes = tempImage.EncodeToPNG();
		tempImage.LoadImage(bytes);

		Object.Destroy(tempImage);

		File.WriteAllBytes(Application.dataPath + "/../SavedScreen.png", bytes);

		yield return new WaitForEndOfFrame();

	}


	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			StartCoroutine("UploadPNG");
			print("Scatta Foto e salva in locale");

		}
	}
}
