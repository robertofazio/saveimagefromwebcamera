# Encode .PNG images from Web Camera #

Simple sketch that allows you to save images into a local folder from the Unity's WebCameraTexture Class. 
Made for some IED Firenze students during their project thesis.

### Unity3D 5.4.3 Tested on OSX and Linux ###

* In Unity Editor you'll find the .png encoded in /Resource folder
* In build you have to open the .app and you'll find the .png into it

### How it works? ###

* just press space bar in order to save the image